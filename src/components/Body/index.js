import React, { useState } from 'react';
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { actionStoreUsersRequest } from '../../store/modules/user/actions';

const Body = () => {
  const [count, setCount] = useState(0);
  const dispatch = useDispatch();

  const { loading } = useSelector(state => state.users);

  const increment = () => {
    setCount(count + 1);
  };

  const callGithubApi = () => {
    dispatch(
      actionStoreUsersRequest({
        name: 'Mateus Santos',
        age: '24',
      }),
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {loading && <ActivityIndicator size="large" color="#000" />}
      <Text style={styles.text}>{count}</Text>

      <TouchableOpacity onPress={increment}>
        <Text style={styles.text}>Incrementar</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={callGithubApi}>
        <Text style={styles.text}>Chamar API GitHub</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 30,
  },
});

export default Body;
