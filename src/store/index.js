import { persistStore } from 'redux-persist';
import createSagaMiddleware from '@redux-saga/core';

import createStore from './createStore';
import persistReducers from './persistReducers';
import rootReducer from './modules/rootReducer';
import rootSaga from './modules/rootSaga';

// Reactotron
const sagaMonitor = __DEV__ ? console.tron.createSagaMonitor() : null;

// Middleware do saga
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

// Criamos um array de middlewres
const middlewares = [sagaMiddleware];

// Criamos a store passando os reducers e os middlewares do saga
const store = createStore(persistReducers(rootReducer), middlewares);

// Criamos os persistors
const persistor = persistStore(store);

// Iniciamos os sagas
sagaMiddleware.run(rootSaga);

// Exportamos a store
export { store, persistor };
