import { combineReducers } from 'redux';

import users from './user/reducer';

const appReducer = combineReducers({
  users,
});

const rootReducer = (state, action) => {
  if (action.type === '@CLEAR_REDUCERS') {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
