import { Types } from './types';

export function actionStoreUsersRequest(data) {
  return {
    type: Types.STORE_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionStoreUsersSuccess(data) {
  return {
    type: Types.STORE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function actionStoreUsersFailure(error) {
  return {
    type: Types.STORE_FAILURE,
    payload: {
      error,
    },
  };
}
