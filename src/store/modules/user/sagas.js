import { call, put, all, takeLatest, delay } from 'redux-saga/effects';
import { api } from '../../../services/api';

import { actionStoreUsersSuccess, actionStoreUsersFailure } from './actions';
import { Types } from './types';

export function* storeUserRequest({ payload: { data } }) {
  try {
    // Chamar a API
    // const response = yield call(api.post, '/users', data));
    const response = yield call(api.get, '');

    // Adiciona delay para visualizar o ActivityIndicator
    yield delay(3000);

    yield put(actionStoreUsersSuccess(response.data));
  } catch (error) {
    // Caso falhe
    yield put(actionStoreUsersFailure(error.message));
  }
}

export default all([takeLatest(Types.STORE_REQUEST, storeUserRequest)]);
